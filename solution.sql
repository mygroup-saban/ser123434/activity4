-- use music_db;
USE music_db;

-- #1
SELECT * FROM artists WHERE name LIKE "%d%";

-- #2
SELECT * FROM songs WHERE length < "00:03:30";

-- #3
SELECT albums.album_title, songs.song_name, songs.length 
    FROM albums JOIN songs ON albums.id = songs.album_id;

-- #4
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    WHERE albums.album_title LIKE "%a%";

-- #5
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- #6
SELECT albums.album_title, songs.song_name FROM albums
    JOIN songs ON albums.id = songs.album_id
    ORDER BY albums.album_title DESC;